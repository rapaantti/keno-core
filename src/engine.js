const _ = require('lodash');

const KenoEngine = (()=> {
  const MAX_NUMBER = 70;
  const MIN_NUMBER = 1;
  const DRAW_NUMBERS = 20;

  const range = (start, end) => {
    return new Array(end - start + 1).fill(1).map((_, index) => start + index);
  };

  const getNumbers = () => {
    return _.sortBy(_.take(_.shuffle(range(MIN_NUMBER, MAX_NUMBER)), DRAW_NUMBERS));
  };

  return {
    getNumbers: getNumbers
  }
})();

module.exports = KenoEngine;
